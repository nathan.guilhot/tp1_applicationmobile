package com.example.uapv1705388.tp1;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

class RowHolder extends RecyclerView.ViewHolder
{
    //implements View.OnClickListener{}

    TextView label = null;
    ImageView icon = null;


    RowHolder(View row)
    {
        super(row);

        label = (TextView) row.findViewById(R.id.label);
        icon = (ImageView) row.findViewById(R.id.icon);
    }

    void bindModel (String item)
    {
        label.setText(item);
    }
}
