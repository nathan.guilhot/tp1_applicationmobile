package com.example.uapv1705388.tp1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.CountryItem);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,CountryList.getNameArray());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                Intent intent = new Intent(MainActivity.this,CountryActivity.class);
                Bundle bundle = new Bundle();
                String country= (String) parent.getItemAtPosition(position);
                bundle.putString("Country" , country);
                bundle.putString("Capital" , CountryList.getCountry(country).getmCapital());
                bundle.putString("ImgFile" , CountryList.getCountry(country).getmImgFile());
                bundle.putString("Language" , CountryList.getCountry(country).getmLanguage());
                bundle.putString("Monnaie" , CountryList.getCountry(country).getmCurrency());
                bundle.putInt("Population" , CountryList.getCountry(country).getmPopulation());
                bundle.putInt("Superficie" , CountryList.getCountry(country).getmArea());
                intent.putExtra("Countrybundle",bundle);
                startActivity(intent);
            }
        });

        /*final RecyclerView recyclerview = (RecyclerView) findViewById(R.id.CountryItemR);
        LinearLayoutManager layoutmanager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutmanager);
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerview.setAdapter(new CountryItemAdapter());

        class CountryItemAdapter extends RecyclerView.Adapter<RowHolder>
        {
            @Override
            public RowHolder onCreateViewHolder (ViewGroup parent, int viewType)
            {
                return(new RowHolder(getLayoutInflater().inflate(R.layout.row,parent,false)));
            }

            //@Override
            public void onBindViewHolder(RowHolder holder,int position)
            {
                holder.bindModel(CountryList.getNameArray()[position]);
            }

            @Override
            public int getItemCount()
            {
                return(CountryList.getNameArray().length);
            }
        }*/
    }
}
