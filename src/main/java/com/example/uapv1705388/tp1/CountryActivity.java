package com.example.uapv1705388.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Intent intent = getIntent();
        final Bundle bundle = intent.getBundleExtra("Countrybundle");

        TextView viewcountryname = (TextView) findViewById(R.id.CountryName);
        viewcountryname.setText(bundle.getString("Country"));

        ImageView viewcountryflag = (ImageView) findViewById(R.id.CountryFlag);
        int drawablecountryflag = viewcountryflag.getContext().getResources().getIdentifier(bundle.getString("ImgFile"), "drawable", viewcountryflag.getContext().getPackageName());
        viewcountryflag.setImageResource(drawablecountryflag);

        final EditText viewcapital = (EditText) findViewById(R.id.Capital);
        viewcapital.setText(bundle.getString("Capital"));

        final EditText viewlangueoff = (EditText) findViewById(R.id.LangueOff);
        viewlangueoff.setText(bundle.getString("Language"));

        final EditText viewmonnaie = (EditText) findViewById(R.id.Monnaie);
        viewmonnaie.setText(bundle.getString("Monnaie"));

        final EditText viewsuperficie = (EditText) findViewById(R.id.Superficie);
        viewsuperficie.setText(Integer.toString(bundle.getInt("Superficie")));

        final EditText viewpopulation = (EditText) findViewById(R.id.Population);
        viewpopulation.setText(Integer.toString(bundle.getInt("Population")));

        Button viewsavebutton = (Button) findViewById(R.id.SaveButton);

        viewsavebutton.setOnClickListener(new AdapterView.OnClickListener()
        {
            public void onClick(View v)
            {
                CountryList.setCountry(bundle.getString("Country"),new Country(
                        viewcapital.getText().toString(),
                        bundle.getString("ImgFile"),
                        viewlangueoff.getText().toString(),
                        viewmonnaie.getText().toString(),
                        Integer.parseInt(viewpopulation.getText().toString()),
                        Integer.parseInt(viewsuperficie.getText().toString())

                ));
                Intent intent = new Intent(CountryActivity.this,MainActivity.class);
                finish();
            }
        });

    }
}
